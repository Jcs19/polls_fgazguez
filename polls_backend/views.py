""" Cornice services.
"""
from cornice import Service
import datetime
hello1 = Service(name='hello1', path='/test', description ="testing")

@hello1.get()
def get_info(request):
    return str(datetime.datetime.now())


hello = Service(name='hello', path='/', description="Simplest app")

@hello.get()
def get_info(request):
    """Returns Hello in JSON."""
    return {'Hello': 'World'}

@hello.post()
def post_info(request):

    return


